package br.mt.gov.indea.sindesaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SindesaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SindesaApiApplication.class, args);
	}

}
